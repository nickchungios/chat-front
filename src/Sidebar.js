import { Avatar, IconButton } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import "./Sidebar.css";
import SearchIcon from '@material-ui/icons/Search';
// import RateReviewOutlined from '@material-ui/icons/RateReviewOutlined';
import HomeIcon from '@material-ui/icons/Home';
import { selectUser } from './features/userSlice';
import db, { auth } from './firebase';

// Components
import SideBarChat from './SidebarChat';

function SideBar() {

  const user = useSelector(selectUser);
  const [chats, setChats] = useState([]);

  useEffect(() => {
    db.collection("chats").onSnapshot((snapshot) => {
      setChats(
        snapshot.docs.map((doc) => ({
          id: doc.id,
          data: doc.data(),
        }))
      )
    });
  }, []);

  // const addChat = () => {

  //   const chatName = prompt('Please enter a chat name.');

  //   if (chatName) {
  //     db.collection('chats').add({
  //       chatName: chatName,
  //     });
  //   }
  // };

  return (
    <div className="sidebar">
      <div className="sidebar__header">
        <Avatar
          className="sidebar__avatar"
          onClick={() => { auth.signOut() }}
          src={user.photo} />
        <div className="sidebar__input">
          <SearchIcon />
          <input placeholder="Search" />
        </div>
        <IconButton variant="outlined" className="sidebar__inputButton">
          {/* <RateReviewOutlined onClick={addChat} />
           */}
          <HomeIcon onClick={() => { window.location.href = 'https://portfolio.nickchunglolz.com/' }} />
        </IconButton>
      </div>
      <div className="sidebar__chats">
        {chats.map(({ id, data: { chatName } }) => (
          <SideBarChat key={id} id={id} chatName={chatName} />
        ))}
      </div>
    </div>
  )
}

export default SideBar;