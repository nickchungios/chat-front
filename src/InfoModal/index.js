import React from 'react';
import { useSelector } from 'react-redux';
import { selectChatName } from '../features/chatSlice';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';

// CSS
import './index.css';

// UI
import { IconButton } from '@material-ui/core';
// Icons
import InfoOutlinedIcon from '@material-ui/icons/InfoOutlined';

const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

export default function InfoModal() {
  const classes = useStyles();
  const chatName = useSelector(selectChatName);
  const [open, setOpen] = React.useState(false);

  const descData = {
    'Bot': (
      <p id="transition-modal-description">Hi! I am Nick's chatbot Nicky, that made by Pytorch, NLTK, and AWS Lambda.</p>
    )
  }

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div className="info_modal">
      <IconButton className="icon__btn" onClick={handleOpen}>
        <InfoOutlinedIcon className="chat__info" />
      </IconButton>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <div className={classes.paper}>
            <h2 id="transition-modal-title">{chatName}</h2>
            {descData[chatName]}
          </div>
        </Fade>
      </Modal>
    </div>
  );
}