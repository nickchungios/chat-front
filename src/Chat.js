import { IconButton } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import axios from 'axios';
import { v4 as uuidv4 } from 'uuid';
import './Chat.css';
import { selectChatId, selectChatName } from './features/chatSlice';
import { selectUser } from './features/userSlice';

// Components
import Message from './Message';
import Video from './Video';
import InfoModal from './InfoModal/index';

// Icons
import MicNoneIcon from '@material-ui/icons/MicNone';

// Utilties
import db from './firebase';
import firebase from 'firebase';
import FlipMove from 'react-flip-move';
// import socketIOClient from "socket.io-client";
import { useSpeechRecognition } from 'react-speech-kit';
// const ENDPOINT = "http://127.0.0.1:8000";
const ENDPOINT = "https://api.nickchunglolz.com/chatbot-python";


function Chat() {
  const user = useSelector(selectUser);
  const [input, setInput] = useState("");
  const chatName = useSelector(selectChatName);
  const chatId = useSelector(selectChatId);
  const [messages, setMessages] = useState([]);
  // const socket = socketIOClient(ENDPOINT);
  const { listen, stop } = useSpeechRecognition({
    onResult: (result) => {
      setInput(result);
    },
  });

  // socket.on("aaa_response", data => {
  //   db.collection('chats').doc(chatId).collection('messages').add({
  //     timestamp: firebase.firestore.FieldValue.serverTimestamp(),
  //     message: data,
  //     uid: 'bot',
  //     photo: user.photo,
  //     email: 'Bot@nickchunglolz.com',
  //     displayName: 'Bot',
  //   });
  // });

  useEffect(() => {
    // if (chatName === 'Bot') {
    //   let msgs = JSON.parse(localStorage.getItem('botMessages')) || [];
    //   setMessages(msgs);
    // }
    if (chatId) {
      db.collection('chats')
        .doc(chatId)
        .collection('messages')
        .orderBy('timestamp', 'desc')
        .onSnapshot((snapshot) => {
          setMessages(
            snapshot.docs.map((doc) => ({
              id: doc.id,
              data: doc.data()
            }))
          )
        });
    }

  }, [chatId, chatName])

  const sendMessage = (e) => {
    e.preventDefault();

    // if (chatName === 'Bot') socket.emit('aaa', input);
    if (chatName === 'Bot') {
      // let msgs = JSON.parse(localStorage.getItem('botMessages')) || [];
      // msgs.push({
      //   id: uuidv4(),
      //   data: {
      //     timestamp: new Date().getTime(),
      //     message: input,
      //     uid: user.uid,
      //     photo: user.photo,
      //     email: user.email,
      //     displayName: user.displayName,
      //   }
      // });
      // localStorage.setItem('botMessages', JSON.stringify(msgs));
      // setMessages(msgs);

      db.collection('chats').doc(chatId).collection('messages').add({
        timestamp: firebase.firestore.FieldValue.serverTimestamp(),
        message: input,
        uid: user.uid,
        photo: user.photo,
        email: user.email,
        displayName: user.displayName,
      });

      axios.post(ENDPOINT, {
        question: input,
      })
        .then(function (res) {
          let ans = res.data;
          console.log(ans);
          // msgs.push({
          //   id: uuidv4(),
          //   data: {
          //     timestamp: new Date().getTime(),
          //     message: ans,
          //     uid: 'bot',
          //     photo: 'https://lh3.googleusercontent.com/-S_52ioQGXGg/AAAAAAAAAAI/AAAAAAAAAAA/AMZuuckhlKfI2Ybl6nwZJjjsvhj00bB8fw/s96-c/photo.jpg',
          //     email: 'Bot@nickchunglolz.com',
          //     displayName: 'Bot',
          //   }
          // });
          // localStorage.setItem('botMessages', JSON.stringify(msgs));
          // setMessages(msgs);
          db.collection('chats').doc(chatId).collection('messages').add({
            timestamp: firebase.firestore.FieldValue.serverTimestamp(),
            message: ans['body'],
            uid: 'bot',
            photo: 'https://lh3.googleusercontent.com/-S_52ioQGXGg/AAAAAAAAAAI/AAAAAAAAAAA/AMZuuckhlKfI2Ybl6nwZJjjsvhj00bB8fw/s96-c/photo.jpg',
            email: 'Bot@nickchunglolz.com',
            displayName: 'Bot',
          });
        })
        .catch(function (err) {
          console.log(err);
          alert(err);
        });
    } else {
      // db.collection('chats').doc(chatId).collection('messages').add({
      //   timestamp: firebase.firestore.FieldValue.serverTimestamp(),
      //   message: input,
      //   uid: user.uid,
      //   photo: user.photo,
      //   email: user.email,
      //   displayName: user.displayName,
      // });
    }

    setInput("");
  };

  let chatComponent = (
    <div className="chat">
      { /* Chat Header */}
      <div className="chat__header">
        <h4>
          To: <span className="chat__name">{chatName}</span>
        </h4>
        <strong>
          <InfoModal />
        </strong>
      </div>
      { /* Chat Message */}
      <div className="chat__messages">
        <FlipMove>
          {messages.map(({ id, data }) => (
            <Message key={id} contents={data} />
          ))}
        </FlipMove>
      </div>
      { /* Chat Input */}
      <div className="chat__input">
        <div className="chat__textField">
          <input
            value={input}
            onChange={(e) => setInput(e.target.value)}
            placeholder="Press Enter..."
            type="text"
            onKeyPress={e => { if (e.code === "Enter" || e.code === "NumpadEnter") sendMessage(e) }}
          />
          <button type="submit" onClick={sendMessage}>Send Message</button>
        </div>

        <IconButton onMouseDown={listen} onMouseUp={stop}>
          <MicNoneIcon className="chat__mic" />
        </IconButton>

      </div>
    </div >
  )

  if (chatName === 'Video(WebRTC)') chatComponent = (
    <Video></Video>
  )

  return chatComponent;

}

export default Chat;