import { Button } from '@material-ui/core';
import React from 'react';
import { auth, provider } from './firebase';
import './Login.css';

function Login() {

  const signIn = () => {
    auth.signInWithPopup(provider)
      .catch((err) => {
        alert(err.message);
      })
  };

  return (
    <div className="login">
      <div className="login_logo">
        <img alt="avatar" src="https://pngimage.net/wp-content/uploads/2018/06/icone-messenger-png-7.png" />
        <h1>ChatBot</h1>
      </div>
      <Button onClick={signIn}>Sign In</Button>
    </div>
  )
}

export default Login