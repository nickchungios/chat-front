import { Avatar } from '@material-ui/core';
import React, { forwardRef } from 'react';
import { useSelector } from 'react-redux';
import { selectUser } from './features/userSlice';
import { selectChatName } from './features/chatSlice';
import './Message.css';

const Message = forwardRef(({
  id,
  contents: { timestamp, displayName, email, message, photo, uid },
}, ref) => {

  const user = useSelector(selectUser);
  const chatName = useSelector(selectChatName);
  // const timestampStr = (chatName === 'Bot') ? timestamp : timestamp?.toDate();
  const timestampStr = timestamp?.toDate();
  return (
    <div ref={ref} className={`message ${user.email === email && "message__sender"}`}>
      <Avatar className="message_photo" src={photo} />
      <p>{message}</p>
      <small>{new Date(timestampStr).toLocaleString()}</small>
    </div>
  )
})

export default Message;
