import React from 'react';
import "./ChatRoom.css";
import SideBar from './Sidebar';
import Chat from './Chat'

function ChatRoom() {
  return (
    <div className="chatRoom">
      <SideBar class="sidebar" />
      <Chat />
    </div>
  )
}

export default ChatRoom;