import firebase from 'firebase';

const firebaseConfig = {
  apiKey: "AIzaSyD9I6OspwLfCM17IIn5G8ToQFymsOerZv0",
  authDomain: "chatbot-react-a64f3.firebaseapp.com",
  projectId: "chatbot-react-a64f3",
  storageBucket: "chatbot-react-a64f3.appspot.com",
  messagingSenderId: "234623793957",
  appId: "1:234623793957:web:02ea0837dbf3e2074cf8cc",
  measurementId: "G-5V005K8ESM"
};

const firebaseApp = firebase.initializeApp(firebaseConfig);
const db = firebaseApp.firestore();
const auth = firebase.auth();
const provider = new firebase.auth.GoogleAuthProvider();

export { auth, provider };
export default db;