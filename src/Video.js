import React, { useRef, } from 'react';

import db from './firebase';

// Style
import './Video.css';


function Video() {

  const servers = {
    iceServers: [
      {
        urls: ['stun:stun1.l.google.com:19302', 'stun:stun2.l.google.com:19302'],
      },
    ],
    iceCandidatePoolSize: 10,
  };

  const pc = new RTCPeerConnection(servers);
  let localStream = null;
  let remoteStream = null;

  // HTML elements
  const webcamButton = useRef(null);
  const webcamVideo = useRef(null);
  const callButton = useRef(null);
  const callInput = useRef(null);
  const answerButton = useRef(null);
  const remoteVideo = useRef(null);
  const hangupButton = useRef(null);

  let webcamStart = async (e) => {
    e.preventDefault();

    localStream = await navigator.mediaDevices.getUserMedia({ video: true, audio: true });
    remoteStream = new MediaStream();

    // Push tracks from local stream to peer connection
    localStream.getTracks().forEach((track) => {
      pc.addTrack(track, localStream);
    });

    // Pull tracks from remote stream, add to video stream
    pc.ontrack = (event) => {
      event.streams[0].getTracks().forEach((track) => {
        remoteStream.addTrack(track);
      });
    };

    webcamVideo.current.srcObject = localStream;
    remoteVideo.current.srcObject = remoteStream;

    callButton.current.disabled = false;
    answerButton.current.disabled = false;
    webcamButton.current.disabled = true;

  }

  let call = async (e) => {
    e.preventDefault();

    // Reference Firestore collections for signaling
    const callDoc = db.collection('calls').doc();
    const offerCandidates = callDoc.collection('offerCandidates');
    const answerCandidates = callDoc.collection('answerCandidates');

    callInput.current.value = callDoc.id;

    // Get candidates for caller, save to db
    pc.onicecandidate = (event) => {
      event.candidate && offerCandidates.add(event.candidate.toJSON());
    };

    // Create offer
    const offerDescription = await pc.createOffer();
    await pc.setLocalDescription(offerDescription);

    const offer = {
      sdp: offerDescription.sdp,
      type: offerDescription.type,
    };

    await callDoc.set({ offer });

    // Listen for remote answer
    callDoc.onSnapshot((snapshot) => {
      const data = snapshot.data();
      if (!pc.currentRemoteDescription && data?.answer) {
        const answerDescription = new RTCSessionDescription(data.answer);
        pc.setRemoteDescription(answerDescription);
      }
    });

    // When answered, add candidate to peer connection
    answerCandidates.onSnapshot((snapshot) => {
      snapshot.docChanges().forEach((change) => {
        if (change.type === 'added') {
          const candidate = new RTCIceCandidate(change.doc.data());
          pc.addIceCandidate(candidate);
        }
      });
    });

    hangupButton.current.disabled = false;
  }

  let answer = async (e) => {

    const callId = callInput.current.value;
    const callDoc = db.collection('calls').doc(callId);
    const answerCandidates = callDoc.collection('answerCandidates');
    const offerCandidates = callDoc.collection('offerCandidates');

    pc.onicecandidate = (event) => {
      event.candidate && answerCandidates.add(event.candidate.toJSON());
    };

    const callData = (await callDoc.get()).data();

    const offerDescription = callData.offer;
    await pc.setRemoteDescription(new RTCSessionDescription(offerDescription));

    const answerDescription = await pc.createAnswer();
    await pc.setLocalDescription(answerDescription);

    const answer = {
      type: answerDescription.type,
      sdp: answerDescription.sdp,
    };

    await callDoc.update({ answer });

    offerCandidates.onSnapshot((snapshot) => {
      snapshot.docChanges().forEach((change) => {
        console.log(change);
        if (change.type === 'added') {
          let data = change.doc.data();
          pc.addIceCandidate(new RTCIceCandidate(data));
        }
      });
    });
  }

  return (
    <div className="video">
      <div className="screen">
        <h2>1. Start your Webcam</h2>
        <div className="cams">
          <span>
            <h3>Local Stream</h3>
            <video id="self__cam" ref={webcamVideo} autoPlay playsInline></video>
          </span>
          <span>
            <h3>Remote Stream</h3>
            <video id="remote__cam" ref={remoteVideo} autoPlay playsInline></video>
          </span>
        </div>
        <button id="webcam__btn" onClick={webcamStart} ref={webcamButton} >Start webcam</button>
      </div>
      <div className="connections">
        <h2>2. Create a new Call</h2>
        <fieldset ref={callButton} disabled>
          <button id="call__btn" onClick={call}>Create Call (offer)</button>
        </fieldset>

        <h2>3. Join a Call</h2>
        <p>Answer the call from a different browser window or device</p>

        <fieldset ref={answerButton} disabled>
          <input id="call__input" ref={callInput} />
          <button id="answer__btn" onClick={answer} >Answer</button>
        </fieldset>

        <h2>4. Hangup</h2>

        <fieldset ref={hangupButton} disabled>
          <button id="hangup__btn" onClick={() => { pc.close() }}>Hangup</button>
        </fieldset>

      </div>
    </div>
  )
}

export default Video;